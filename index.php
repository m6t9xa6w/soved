<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="theme-color" content="#2980b9">
		<title>Soved</title>
		<!--
			Soved v<?php require '.semver';?>

			Copyright 2015 Ucfirst
			https://ucfirst.nl/
		-->
		<meta name="author" content="Sander de Vos">
		<meta name="description" content="The online business card of Sander de Vos.">
		<meta name="keywords" content="Sander de Vos, business card, Soved">
		<link href="dist/css/soved.min.css" rel="stylesheet">
	</head>
	<body>
		<div class="background"></div>
		<div class="container-fluid">
			<div class="row vertical-offset-20 animated fadeInLeftBig" id="business-card">
				<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
					<div class="panel panel-primary">
						<div class="panel-body">
							<img src="dist/img/Sander.jpg" alt="A profile picture of Sander" class="center-block img-circle" width="150">
							<h1 class="text-center">Sander de Vos</h1>
							<h2 class="text-center"><small>Backend Developer @ <a href="https://ucfirst.nl/" target="_blank">Ucfirst</a> &amp; <a href="http://lastmason.com/" target="_blank">LastMason</a></small></h2>
							<div class="container-fluid">
								<div class="row vertical-offset-20">
									<div class="col-xs-3">
										<a href="https://twitter.com/sdevos16" target="_blank" class="fa fa-twitter fa-5x"></a>
									</div>
									<div class="col-xs-3">
										<a href="mailto:sander@tutanota.de" class="fa fa-envelope-o fa-5x"></a>
									</div>
									<div class="col-xs-3">
										<a href="skype:live:sander3_7?call" class="fa fa-skype fa-5x"></a>
									</div>
									<div class="col-xs-3">
										<a href="https://bitbucket.org/m6t9xa6w/soved" target="_blank" class="fa fa-bitbucket fa-5x"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="dist/js/soved.min.js"></script>
	</body>
</html>