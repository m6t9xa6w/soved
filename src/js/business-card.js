$(function() {
	// Start delayed background fadeIn animation
	$('.background').addClass('background-image animated fadeIn');

	var backgrounds = [
		'photo-1422266452180-6fbbb53a973b.jpg',
		'photo-1418479755667-6d5ec7970e21.jpg',
		'photo-1422479516648-9b1f0b6e8da8.jpg',
		'photo-1418479631014-8cbf89db3431.jpg',
		'photo-1413896235942-39c2eb7cd584.jpg',
		'photo-1423439793616-f2aa4356b37e.jpg',
		'photo-1422022098106-b3a9edc463af.jpg',
		'photo-1422480415834-d7d30118ea06.jpg',
		'photo-1422479278902-8cef24992df0.jpg',
		'photo-1422486578093-18e296095a04.jpg'
	];

	// Get the last used backgrounds
	var usedBackgrounds = JSON.parse(localStorage.getItem('usedBackgrounds'));
	if (usedBackgrounds === null) var usedBackgrounds = [];

	// Remove the last used backgrounds from the available backgrounds
	var availableBackgrounds = backgrounds.filter(function(element) {
		return usedBackgrounds.indexOf(element) < 0;
	});

	// Choose a random background
	var backgroundImage = availableBackgrounds[Math.floor(availableBackgrounds.length * Math.random())];

	usedBackgrounds.push(backgroundImage);
	if (usedBackgrounds.length > (backgrounds.length - 1)) usedBackgrounds.shift();

	// Save the last used backgrounds
	localStorage.setItem('usedBackgrounds', JSON.stringify(usedBackgrounds));

	// Use the selected image as background
	var imagePath = 'dist/img/backgrounds/' + backgroundImage;
	$('.background-image').css('background-image', 'url(' + imagePath + ')');

	setTimeout(function() {
		$('.fa-twitter').css('color', '#55acee');
		$('.fa-envelope-o').css('color', '#34495e');
		$('.fa-skype').css('color', '#00aff0');
		$('.fa-bitbucket').css('color', '#205081');
	}, 700);

	// Start business card hinge animation
	$('.text-center a').click(function(event) {
		event.preventDefault();

		$('#business-card').removeClass('fadeInLeftBig fadeIn').addClass('hinge');

		var href = this.href;
		setTimeout(function() {
			window.open(href, '_blank');
			$('#business-card').removeClass('hinge').addClass('fadeIn');
		}, 800);
	});
});