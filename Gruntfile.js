module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: '\n'
			},
			css: {
				src: ['src/css/*.css'],
				dest: 'dist/css/<%= pkg.name %>.css'
			},
			js: {
				src: ['src/js/jquery-2.1.3.min.js', 'src/js/*.js'],
				dest: 'dist/js/<%= pkg.name %>.js'
			}
		},
		cssmin: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			dist: {
				files: {
					'dist/css/<%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
				}
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			dist: {
				files: {
					'dist/js/<%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
				}
			}
		},
		watch: {
			options: {
				spawn: false
			},
			css: {
				files: ['<%= concat.css.src %>'],
				tasks: ['concat', 'cssmin']
			},
			js: {
				files: ['<%= concat.js.src %>'],
				tasks: ['concat', 'uglify']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['concat', 'cssmin', 'uglify']);
	grunt.registerTask('dev', ['concat', 'cssmin', 'uglify', 'watch']);

};