Soved
=====
> The online business card of Sander de Vos

"Soved" is the last name of Sander, but in reverse. This online business card contains the profile picture, job title and contact details of Sander.

## Requirements
* Node.js
* npm
* Grunt
* Apache

## Getting started
* Checkout this project
* Run `npm install` to install project dependencies
* Run `grunt` to concatenate and minify assets

## License
This project is licensed under the terms of the MIT license.